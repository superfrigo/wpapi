<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N'y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oE^!E&&yXd09x8;|glq24t +T<x^dvO|]u8;x9b>]QlL+< c@WP:]p`t[6g/h{t&');
define('SECURE_AUTH_KEY',  'q-hN+p{*+]@-Io?Y|4Wf0K/2WpL[oA$Ojh|/ F&@ext@E$|DZe!~)|K~E8p*zS?n');
define('LOGGED_IN_KEY',    '21p{aV,b,u|++m1L?cw=76 >bbo9-R`I95n*lUeLT5&zLNGB)S28|qq]7K{V|V`$');
define('NONCE_KEY',        'Q1j9`ugPqA_$jJk-]t_* &%2V^_SbM@C#|)#cEf+9NA]YKHpwOtKzLsviMvZ|*=T');
define('AUTH_SALT',        'FfX-Lf?#TRHPkPV3NP8$x/e-A-ST~I}Ng%Z!Zq{e(9Bv@0i?,1_%*i=7X^L-u/m+');
define('SECURE_AUTH_SALT', 'CDi#uj_dd.>NE|YuWig4E}[-D^,N2T{8MHM|cZ&9|~s3[7rM^Q?>>C-Pg4C!xoi(');
define('LOGGED_IN_SALT',   'Qu}I>nC4T9p s;|8(@$Yv@cIl=Ss=[:zo1=#Uw-Q m!5{|L4?t_0JbtZ*#tO4`hK');
define('NONCE_SALT',       'BZ}[8AtGxM|&ZQ@ Ayya!6b<p{:vs::5`+VKS$?!-H/uWi? dmuG}xYN8e8B4-#+');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 */
define('WP_DEBUG', false);

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');